// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "hw_03GameMode.h"
#include "hw_03HUD.h"
#include "hw_03Character.h"
#include "UObject/ConstructorHelpers.h"

Ahw_03GameMode::Ahw_03GameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = Ahw_03HUD::StaticClass();
}
