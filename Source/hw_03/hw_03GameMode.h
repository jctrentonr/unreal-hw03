// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "hw_03GameMode.generated.h"

UCLASS(minimalapi)
class Ahw_03GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	Ahw_03GameMode();
};



